#ifndef INPUT_HPP
#define INPUT_HPP
#include <sdl.h>

enum keysDefined{KUP, KDOWN, KRIGHT, KLEFT, KW, KA, KS, KD, KESC, nkeys};

class input
{

  private:
    //KEY_OFF: The key hasn't been pressed
    //KEY_ON: The key is being pressed
    //KEY_RELEASED: The key has just been released
    //KEY_PRESSED: The key has just been pressed
    enum keyStatus{KEY_OFF, KEY_ON, KEY_RELEASED, KEY_PRESSED};
    int keys[nkeys];
  public:    
    //Constructor
    input();
    //In-out
    void read();
    //Consultant
    bool check(unsigned int key);
    bool checkPressed(unsigned int key);
    bool checkReleased(unsigned int key);
};

#endif