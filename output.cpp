#include "output.h"

//Creator
output::output():screen(NULL){}

//Destructor

output::~output()
{
  ReleaseCOM(mRenderTargetView);
	ReleaseCOM(mDepthStencilView);
	ReleaseCOM(mSwapChain);
	ReleaseCOM(mDepthStencilBuffer);
	ReleaseCOM(md3dDevice);
  ReleaseCOM(mVertexLayout);
  ReleaseCOM(mVertexBuffer);  
  ReleaseCOM(mIndexBuffer);
  ReleaseCOM(mEffect);
}

bool output::Init()
{
  bool initok = false;
  initok = setSdlScreen();
  #if defined DIRECTX
    initok &= setDirectxScreen();
  #endif   
  atexit(SDL_Quit);
  return initok;
}

//Operadoras

//If you want an icon for the screen, it has to be in the same folder that the .exe or .run
//icons must be 32x32
bool output::setSdlScreen()
{  
  if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_NOPARACHUTE) < 0 || !SDL_GetVideoInfo()) return false;
  #if defined DIRECTX
   SDL_putenv("SDL_VIDEODRIVER=directx");    
  #endif
  SDL_Surface* icon = NULL;
  icon = SDL_LoadBMP("icon.bmp");
  if(icon != NULL){
    Uint32 colorkey = SDL_MapRGB(icon->format, 255, 0, 255);
    SDL_SetColorKey(icon, SDL_SRCCOLORKEY, colorkey);
    SDL_WM_SetIcon(icon, NULL); 
    SDL_FreeSurface(icon);   
  } 
  char *vtitle,*vicon; 
  vtitle = "!Gordian"; //Title in the screen caption
  vicon  = "Icono"; //I don't know where this text outputs
  SDL_WM_SetCaption(vtitle, vicon);
  Uint32 flags;
  if(SETFULLSCREEN){
    SDL_ShowCursor(SDL_DISABLE);
    flags             = SDL_ASYNCBLIT | SDL_HWSURFACE | SDL_FULLSCREEN;     
    const SDL_VideoInfo *video_info;
    if(!(video_info = SDL_GetVideoInfo())) return false;
    currentWidth       = video_info->current_w;
    currentHeight      = video_info->current_h;
  }
  else {
    flags             = SDL_ASYNCBLIT | SDL_HWSURFACE | SDL_RESIZABLE;
    currentWidth      = SCREEN_W;
    currentHeight     = SCREEN_H;
  }
  unsigned int bpp    = SDL_GetVideoInfo()->vfmt->BitsPerPixel;  
  this->screen        = SDL_SetVideoMode(currentWidth, currentHeight, bpp, flags);  

  return true;  
}



bool output::setDirectxScreen()
{
  #if defined(DEBUG) | defined(_DEBUG)
	  _CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
  #endif
    
	DXGI_SWAP_CHAIN_DESC swapchain;
	swapchain.BufferDesc.Width                    = currentWidth;
	swapchain.BufferDesc.Height                   = currentHeight;
	swapchain.BufferDesc.RefreshRate.Numerator    = 60;
	swapchain.BufferDesc.RefreshRate.Denominator  = 1;
	swapchain.BufferDesc.Format                   = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapchain.BufferDesc.ScanlineOrdering         = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	swapchain.BufferDesc.Scaling                  = DXGI_MODE_SCALING_UNSPECIFIED;
	// No multisampling.
	swapchain.SampleDesc.Count   = 1;
	swapchain.SampleDesc.Quality = 0;

	swapchain.BufferUsage  = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapchain.BufferCount  = 1;
	swapchain.OutputWindow = GetActiveWindow();
	swapchain.Windowed     = !SETFULLSCREEN;
	swapchain.SwapEffect   = DXGI_SWAP_EFFECT_DISCARD;
	swapchain.Flags        = 0;

  md3dDevice          = 0;
	mSwapChain          = 0;
	mDepthStencilBuffer = 0;
	mRenderTargetView   = 0;
	mDepthStencilView   = 0;
  mVertexLayout       = 0;
  mVertexBuffer       = 0;
  mIndexBuffer        = 0;
  mClearColor         = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
  mEffect             = 0;
  mTechnique          = 0;   
                    
  D3D10_DRIVER_TYPE md3dDriverType  = D3D10_DRIVER_TYPE_HARDWARE;	

	UINT createDeviceFlags = 0;
  #if defined(DEBUG) || defined(_DEBUG)  
      createDeviceFlags |= D3D10_CREATE_DEVICE_DEBUG;
  #endif

	D3D10CreateDeviceAndSwapChain(
			0,                 //default adapter
			md3dDriverType,
			0,                 // no software device
			createDeviceFlags, 
			D3D10_SDK_VERSION,
			&swapchain,
			&mSwapChain,
			&md3dDevice);

  //Shaders
  DWORD dwShaderFlags = D3D10_SHADER_ENABLE_STRICTNESS;    
  #if defined( DEBUG ) || defined( _DEBUG )
    dwShaderFlags |= D3D10_SHADER_DEBUG;
  #endif
  HRESULT hr; //Used to check everything went ok (Windows only)
  hr = D3DX10CreateEffectFromFile( "Tutorial02.fx", NULL, NULL, "fx_4_0", dwShaderFlags, 0,
                                         md3dDevice, NULL, NULL, &mEffect, NULL, NULL );
  if( FAILED( hr ) ) {
     cout << "Fx file hasn't been found" << endl;
     return false;
  }
  mTechnique          = mEffect->GetTechniqueByName( "Render" );
  mWorldVariable      = mEffect->GetVariableByName( "World" )->AsMatrix();
  mViewVariable       = mEffect->GetVariableByName( "View" )->AsMatrix();
  mProjectionVariable = mEffect->GetVariableByName( "Projection" )->AsMatrix();

  D3D10_INPUT_ELEMENT_DESC layout[] =  {
      { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D10_INPUT_PER_VERTEX_DATA, 0 },
      { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D10_INPUT_PER_VERTEX_DATA, 0 },
  };
  UINT numElements = sizeof( layout ) / sizeof( layout[0] );
  D3D10_PASS_DESC PassDesc;
  mTechnique->GetPassByIndex( 0 )->GetDesc( &PassDesc );  
  hr = md3dDevice->CreateInputLayout( layout, numElements, PassDesc.pIAInputSignature,
                                        PassDesc.IAInputSignatureSize, &mVertexLayout );
  if( FAILED( hr ) ) return false;
  md3dDevice->IASetInputLayout( mVertexLayout );
  //Vertex buffer 
  SimpleVertex vertices[] =
  {
    { D3DXVECTOR3( -1.0f, 1.0f, -1.0f ), D3DXVECTOR4( 0.0f, 0.0f, 1.0f, 1.0f ) },
    { D3DXVECTOR3( 1.0f, 1.0f, -1.0f ), D3DXVECTOR4( 0.0f, 1.0f, 0.0f, 1.0f ) },
    { D3DXVECTOR3( 1.0f, 1.0f, 1.0f ), D3DXVECTOR4( 0.0f, 1.0f, 1.0f, 1.0f ) },
    { D3DXVECTOR3( -1.0f, 1.0f, 1.0f ), D3DXVECTOR4( 1.0f, 0.0f, 0.0f, 1.0f ) },
    { D3DXVECTOR3( -1.0f, -1.0f, -1.0f ), D3DXVECTOR4( 1.0f, 0.0f, 1.0f, 1.0f ) },
    { D3DXVECTOR3( 1.0f, -1.0f, -1.0f ), D3DXVECTOR4( 1.0f, 1.0f, 0.0f, 1.0f ) },
    { D3DXVECTOR3( 1.0f, -1.0f, 1.0f ), D3DXVECTOR4( 1.0f, 1.0f, 1.0f, 1.0f ) },
    { D3DXVECTOR3( -1.0f, -1.0f, 1.0f ), D3DXVECTOR4( 0.0f, 0.0f, 0.0f, 1.0f ) },
  };
  D3D10_BUFFER_DESC bd;
  D3D10_SUBRESOURCE_DATA InitData;

  bd.Usage = D3D10_USAGE_DEFAULT;
  bd.ByteWidth = sizeof( SimpleVertex ) * 8;
  bd.BindFlags = D3D10_BIND_VERTEX_BUFFER;
  bd.CPUAccessFlags = 0;
  bd.MiscFlags = 0;
  //Indica los datos del buffer (hay 16 buffers)
  InitData.pSysMem = vertices;
  hr = md3dDevice->CreateBuffer( &bd, &InitData, &mVertexBuffer );
  if( FAILED( hr ) ) return false;
  UINT stride = sizeof( SimpleVertex );
  UINT offset = 0;
  md3dDevice->IASetVertexBuffers( 0, 1, &mVertexBuffer, &stride, &offset );  
  //Index buffer
  DWORD indices[] =
  {
      3,1,0,
      2,1,3,

      0,5,4,
      1,5,0,

      3,4,7,
      0,4,3,

      1,6,5,
      2,6,1,

      2,7,6,
      3,7,2,

      6,4,5,
      7,4,6,
  };
  
  bd.Usage = D3D10_USAGE_DEFAULT;
  bd.ByteWidth = sizeof( DWORD ) * 36;
  bd.BindFlags = D3D10_BIND_INDEX_BUFFER;
  bd.CPUAccessFlags = 0;
  bd.MiscFlags = 0;
  //Indica los datos del buffer (hay 16 buffers)
  InitData.pSysMem = indices;
  hr = md3dDevice->CreateBuffer( &bd, &InitData, &mIndexBuffer );
  if( FAILED( hr ) ) return false;
  md3dDevice->IASetIndexBuffer( mIndexBuffer, DXGI_FORMAT_R32_UINT, 0);  
  
  md3dDevice->IASetPrimitiveTopology( D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST );

  D3DXMatrixIdentity( &mWorld ); //You don't have to change the coordinates

  //Camera
  D3DXVECTOR3 Eye( 0.0f, 1.0f, -5.0f );
  //Where is looking at
  D3DXVECTOR3 At( 0.0f, 1.0f, 0.0f );
  //Where is +Y
  D3DXVECTOR3 Up( 0.0f, 1.0f, 0.0f );
  D3DXMatrixLookAtLH( &mView, &Eye, &At, &Up );

  D3DXMatrixPerspectiveFovLH( &mProjection, ( float )D3DX_PI * 0.5f, currentWidth / (FLOAT)currentHeight, 0.1f, 100.0f );

	onResize();

  return true;
}

void output::onResize()
{
	// Release the old views, as they hold references to the buffers we
	// will be destroying.  Also release the old depth/stencil buffer.
	ReleaseCOM(mRenderTargetView);
	ReleaseCOM(mDepthStencilView);
	ReleaseCOM(mDepthStencilBuffer);
	// Resize the swap chain and recreate the render target view.
	mSwapChain->ResizeBuffers(1, currentWidth, currentHeight, DXGI_FORMAT_R8G8B8A8_UNORM, 0);
	ID3D10Texture2D* backBuffer;
	mSwapChain->GetBuffer(0, __uuidof(ID3D10Texture2D), reinterpret_cast<void**>(&backBuffer));
	md3dDevice->CreateRenderTargetView(backBuffer, 0, &mRenderTargetView);
	ReleaseCOM(backBuffer);
	// Create the depth/stencil buffer and view.
	D3D10_TEXTURE2D_DESC depthStencilDesc;
	
	depthStencilDesc.Width              = currentWidth;
	depthStencilDesc.Height             = currentHeight;
	depthStencilDesc.MipLevels          = 1;
	depthStencilDesc.ArraySize          = 1;
	depthStencilDesc.Format             = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilDesc.SampleDesc.Count   = 1; // multisampling must match
	depthStencilDesc.SampleDesc.Quality = 0; // swap chain values.
	depthStencilDesc.Usage              = D3D10_USAGE_DEFAULT;
	depthStencilDesc.BindFlags          = D3D10_BIND_DEPTH_STENCIL;
	depthStencilDesc.CPUAccessFlags     = 0; 
	depthStencilDesc.MiscFlags          = 0;

	md3dDevice->CreateTexture2D(&depthStencilDesc, 0, &mDepthStencilBuffer);
	md3dDevice->CreateDepthStencilView(mDepthStencilBuffer, 0, &mDepthStencilView);
	// Bind the render target view and depth/stencil view to the pipeline.
	md3dDevice->OMSetRenderTargets(1, &mRenderTargetView, mDepthStencilView);
	// Set the viewport transform.
	D3D10_VIEWPORT vp;

	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	vp.Width    = currentWidth;
	vp.Height   = currentHeight;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;

	md3dDevice->RSSetViewports(1, &vp);  
 }

void output::drawScene()
{
  md3dDevice->ClearRenderTargetView(mRenderTargetView, mClearColor);
	md3dDevice->ClearDepthStencilView(mDepthStencilView, D3D10_CLEAR_DEPTH|D3D10_CLEAR_STENCIL, 1.0f, 0);

  //Render stuff!
  mWorldVariable->SetMatrix( (float*)&mWorld );
  mViewVariable->SetMatrix( (float*)&mView );
  mProjectionVariable->SetMatrix( (float*)&mProjection );

  D3D10_TECHNIQUE_DESC techDesc;
  mTechnique->GetDesc( &techDesc );
  for( UINT p = 0; p < techDesc.Passes; ++p )  {
    mTechnique->GetPassByIndex( p )->Apply( 0 );
    md3dDevice->DrawIndexed( 36, 0, 0 );
  }
  mSwapChain->Present( 0, 0 ); //Draw the backbuffer
}


int output::Main()
{
  // INIT //
  if(!Init()) {
    cerr << "Pantalla no cargada" << endl;
    return 1;
  }
  //Metropolis_effect_INIT();  
  
  input i;
  float x,y,z;
  x = 0.0f;
  y = 1.0f;
  z = -5.0f;
  //Where is looking at
  D3DXVECTOR3 At( 0.0f, 1.0f, 0.0f );
  //Where is +Y
  D3DXVECTOR3 Up( 0.0f, 1.0f, 0.0f );
  D3DXVECTOR3 Eye( x, y, z );
  while(!i.check(KESC)) {
    // INPUT //
    i.read();    

    // LOGIC //
    if(i.check(KUP)){
      //Camera
      y = y + 0.01f;
      Eye.x = x;
      Eye.y = y;
      Eye.z = z;
      D3DXMatrixLookAtLH( &mView, &Eye, &At, &Up );
    }
    if(i.check(KDOWN)) {
      //Camera
      y = y - 0.01f;
      Eye.x = x;
      Eye.y = y;
      Eye.z = z;
      D3DXMatrixLookAtLH( &mView, &Eye, &At, &Up );
    }
    if(i.check(KRIGHT)) {
      //Camera
      x = x + 0.01f;
      Eye.x = x;
      Eye.y = y;
      Eye.z = z;
      D3DXMatrixLookAtLH( &mView, &Eye, &At, &Up );
    }
    if(i.check(KLEFT)) {
      //Camera
      x = x - 0.01f;
      Eye.x = x;
      Eye.y = y;
      Eye.z = z;
      D3DXMatrixLookAtLH( &mView, &Eye, &At, &Up );
    }
    if(i.check(KW)) {
      //Camera
      z = z + 0.01f;
      Eye.x = x;
      Eye.y = y;
      Eye.z = z;
      D3DXMatrixLookAtLH( &mView, &Eye, &At, &Up );
    }
    if(i.check(KA)) {
      //Camera
      x = x + 0.01f;
      y = y + 0.01f;
      Eye.x = x;
      Eye.y = y;
      Eye.z = z;
      D3DXMatrixLookAtLH( &mView, &Eye, &At, &Up );
    }
    if(i.check(KS)) {
      //Camera
      z = z - 0.01f;
      Eye.x = x;
      Eye.y = y;
      Eye.z = z;
      D3DXMatrixLookAtLH( &mView, &Eye, &At, &Up );
    }
    if(i.check(KD))  {
      //Camera
      x = x - 0.01f;
      y = y - 0.01f;
      Eye.x = x;
      Eye.y = y;
      Eye.z = z;
      D3DXMatrixLookAtLH( &mView, &Eye, &At, &Up );
    }

	  // DRAW / RENDER //
    //Metropolis_effect_DISPLAY();
	  drawScene();  
  }
  
  SDL_Quit(); 
  return 0;
}