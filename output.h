#ifndef OUTPUT_HPP
#define OUTPUT_HPP
#define DIRECTX 
//Macro to release com interfaces
#define ReleaseCOM(x) { if(x){ x->Release();x = 0; } }
//#define OPENGL

#include <sdl.h>
#include <vector>
#include <iostream>
#if defined DIRECTX
  #include <d3dx10.h>
  #include <dxerr.h>
  #include <Windows.h>
  #include "structs.hpp"
#endif
#include "input.h"

using namespace std;

//---------------------------------------------
// GLOBAL AND CONSTANT VARIABLES
//---------------------------------------------
const unsigned int SCREEN_W                     = 1024;
const unsigned int SCREEN_H                     = 800;
const bool         SETFULLSCREEN                = false;


class output
{
  private: 
    #if defined DIRECTX
      ID3D10Device*           md3dDevice;
	    IDXGISwapChain*         mSwapChain;
	    ID3D10Texture2D*        mDepthStencilBuffer;
	    ID3D10RenderTargetView* mRenderTargetView;
	    ID3D10DepthStencilView* mDepthStencilView;
      ID3D10InputLayout*      mVertexLayout;
      ID3D10Buffer*           mVertexBuffer;
      ID3D10Buffer*           mIndexBuffer;
      //Constant matrices for shaders
ID3D10EffectMatrixVariable* mWorldVariable;
ID3D10EffectMatrixVariable* mViewVariable  ;
ID3D10EffectMatrixVariable* mProjectionVariable;
D3DXMATRIX                  mWorld;
D3DXMATRIX                  mView;
D3DXMATRIX                  mProjection;
      //Backgrund color
      D3DXCOLOR               mClearColor;      
      //Shader stuff
      ID3D10Effect*           mEffect;
      ID3D10EffectTechnique*  mTechnique;
    #endif
    SDL_Surface* screen;
    unsigned int currentWidth,currentHeight;

    bool setSdlScreen();
    bool setDirectxScreen(); 
    void onResize(); 
    void drawScene(); 

  public:
    //Creator
    output();
    //Destructor 
    ~output();
    //Operators
    bool Init();
    int Main();    
};

#endif